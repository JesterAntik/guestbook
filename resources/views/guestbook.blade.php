<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{config('app.name')}}</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
          integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">

    <!-- Styles -->
    <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"
            integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"
            integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb"
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"
            integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn"
            crossorigin="anonymous"></script>
</head>
<body>
<div class="flex-center position-ref full-height">
    <div class="content">
        <nav class="navbar navbar-light bg-faded">
            <a class="navbar-brand" href="{{route('home')}}">Guest Book</a>
        </nav>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="row no-gutters">
            <div class="col-7">
                <h3>Отзывы</h3>
                <ul class="list-group m-2">
                    @foreach($comments as $comment)
                        <li class="list-group-item">
                            <blockquote class="blockquote">
                                <p class="mb-0">{{$comment->body}}</p>
                                <footer class="blockquote-footer"><cite title="Source Title">{{$comment->name}}</cite></footer>
                            </blockquote>
                        </li>
                    @endforeach
                </ul>
            </div>
            <div class="col-5">
                <h3>Новый отзыв</h3>
                <form action="{{route('comments.store')}}" method="post" class="m-2">
                    @csrf
                    <div class="form-group">
                        <label for="name">Имя: </label>
                        <input id="name" type="text" name="name" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="body">Отзыв: </label>
                        <textarea id="body" name="body" class="form-control" rows="3"></textarea>
                    </div>
                    <button type="submit" class="btn btn-sm btn-success">Отправить</button>
                </form>
            </div>
        </div>
        <div class="comments">


        </div>

        <div class="new-comment">

        </div>
    </div>
</div>
</body>
</html>
