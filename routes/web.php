<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Http\Request;
use App\Comment;
Route::get('/', function () {
    $comments=Comment::all();
    return view('guestbook')->with(compact('comments'));
})->name('home');

Route::post('/comments', function(Request $request){
    $validatedData = $request->validate([
        'name' => 'required|string|max:120',
        'body' => 'required',
    ]);
    $comment = Comment::create($validatedData);
    return redirect()->route('home');
})->name('comments.store');